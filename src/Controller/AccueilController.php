<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Salon;
use App\Entity\Video;
use App\Entity\Utilisateur;
use App\Entity\User;
use App\Entity\Tag;
use App\Entity\Recommendation;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\RejoindreType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
//use App\Controller\SalleController;

class AccueilController extends AbstractController {

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/api/recherche/{tag}", methods={"GET"})
     */
    public function AjaxGetResultats(Request $request, $tag){

        $repo = $this->getDoctrine()->getRepository(Recommendation::class);
        $resultats = $repo->findByTags($tag);
        
        if ($resultats == NULL) {

            dump("NULL");

            return new Response("[]");
        }

        dump("OK");
        dump($resultats);

        $parameter = array();

        foreach($resultats as $resultat) {
            array_push($parameter, array("Lien" => $resultat->getLien(),
                                        "Nom" => $resultat->getNom()));
        }

        return new Response(json_encode(array('resultats' => $parameter)));
    }

    public function accueil(Request $request) {

        // Formulaire rejoindre salon

        $form = $this->createFormBuilder()
            ->add('identifiantSalon', TextType::class)
            ->add('valider', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->getData()["identifiantSalon"] !== NULL){

            $value = $form->getData();

            $this->UserManagement();

            //dump(1);

            $salon = $this->getDoctrine()->getRepository(Salon::class)->findOneBy(['id' => $value['identifiantSalon']]);
            //Si un salon est trouvé on va dessus sinon redirection sur la page d'accueil
            if(isset($salon)){
                //return $this->render('salle.html.twig', array('salon' => $salon)); // ou ['numero'=>$nombre]);
                return $this->redirectToRoute('salle', array("salon" => $salon->getId()));
            }
            else{
                return $this->redirectToRoute('accueil');
            }
        } else if (isset($_GET['pseudonyme']) && isset($_GET['lienYoutube'])){

            $pseudonyme = $_GET['pseudonyme'];
            $lienYoutube = $_GET['lienYoutube'];

            $idUser = $this->UserManagement();

            $user = $this->getDoctrine()->getRepository(Utilisateur::class)->findOneBy(['id' => $idUser]);
            $user->setPseudo($pseudonyme);
            $entityManager = $this->getDoctrine()->getManager();
            $salon = new Salon();
            $codeSalon = random_int(100000, 999999);
            $salon->setCodeSalon($codeSalon);
            $salon->setDateCreation(new \DateTime());
            $salon->setHeureDernierPing(new \DateTime());
            $salon->setEstSupprime(false);
            $salon->setChef($idUser);
            $salon->setLienVideoEnCours($lienYoutube);
            $entityManager->persist($salon);
            $entityManager->flush();
            $video = new Video();
            $video->setIdSalon($salon->getId());
            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('salle', array("salon" => $salon->getId()));
        } 

        $listeTags = $this->getDoctrine()->getRepository(Tag::class)->findAll();

        return $this->render('accueil.html.twig', array('form' => $form->createView(),
                                                        'listeTags' => $listeTags));
    }

    //Gestion de la vérification et création d'un utilisateur en session
    private function UserManagement(){
        $idUser = $this->session->get('identifiant');
        if(!isset($idUser)|| $idUser == ''){
            //$randomId = uniqid('user_');
            $entityManager = $this->getDoctrine()->getManager();
            $user = new Utilisateur();
            $user->setPseudo(uniqid('user_'));
            $user->setDistantCode("");

            $entityManager->persist($user);
            $entityManager->flush();

            $this->session->set('identifiant', $user->getId());
        }
        return$this->session->get('identifiant');
    }
}