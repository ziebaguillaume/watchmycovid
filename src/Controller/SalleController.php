<?php
namespace App\Controller;
use App\Entity\Salon;
use App\Entity\Video;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Repository\SalonRepository;
use App\Entity\Utilisateur;
use App\Entity\Message;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SalleController extends AbstractController {

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    // ajouter un id de salle
    public function salle($salon, Request $request) {
        $salons = $this->getDoctrine()->getRepository(Salon::class)->findBy(['id' => $salon]);
        $idSalon = "";
        $idAdminSalon = "";
        foreach ($salons as $salon){
            $idSalon = $salon->getId();
            $idAdminSalon = $salon->getChef();
        }
        $idAdmin = $this->getDoctrine()->getRepository(Utilisateur::class)->findBy(['id' => $idAdminSalon]);
        foreach ($idAdmin as $id){
            $idAdmin = $id->getId();
        }
       // print_r($idSalon);
        $session = $request->getSession();

        $idUser = $this->session->get('identifiant');

        return $this->render('salle.html.twig',[
            'idSalon' => $idSalon,
            'id_user'  =>$idUser,
            "video" => $salon->getLienVideoEnCours(),
            'idAdmin' => $idAdmin
        ]);

        return $this->render('salle.html.twig', array("idSalon" => $idSalon));
    }

    public function rejoindreSalon(Request $request){
        $form = $this->createFormBuilder()
            ->add('identifiantSalon', TextType::class)
            ->add('valider', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $value = $form->getData();

            $this->UserManagement();

            $salon = $this->getDoctrine()->getRepository(Salon::class)->findBy(['id' => $value['identifiantSalon']]);
            //Si un salon est trouvé on va dessus sinon redirection sur la page d'accueil
            if(isset($salon)){
                //return $this->render('salle.html.twig', array('salon' => $salon)); // ou ['numero'=>$nombre]);
                return $this->redirectToRoute('salle', array("salon" => $salon->getId(),"video" => $salon->getLienVideoEnCours()));
            }
            else{
                return $this->redirectToRoute('accueil');
            }
        }
        return $this->render('accueil.html.twig', array('form' => $form->createView())); // ou ['numero'=>$nombre]);
    }

    //Quand on appuye sur le bouton création d'un salon alors redirection
    public function createSalon(Request $request){
        ////
        $formCreer = $this->createFormBuilder()
            ->add('pseudonyme', TextType::class)
            ->add('lienYoutube', TextType::class)
            ->add('valider', SubmitType::class)
            ->getForm();
        $formCreer->handleRequest($request);
        if ($formCreer->isSubmitted() && $formCreer->isValid()){
            $value = $formCreer->getData();


            $idUser = $this->UserManagement();
            $entityManager = $this->getDoctrine()->getManager();
            $salon = new Salon();
            $codeSalon = random_int(100000, 999999);
            $salon->setCodeSalon($codeSalon);
            $salon->setDateCreation(new \DateTime());
            $salon->setHeureDernierPing(new \DateTime());
            $salon->setEstSupprime(false);
            $salon->setChef($idUser);
            $salon->setLienVideoEnCours($value['lienYoutube']);
            $entityManager->persist($salon);
            $entityManager->flush();
            $video = new Video();
            $video->setIdSalon($salon->getId());
            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('salle', array("salon" => $salon->getId(),"video" => $salon->getLienVideoEnCours()));
        }
        ////

        return $this->render('accueil.html.twig', array('formCreer' => $formCreer->createView()));
    }

    //Gestion de la vérification et création d'un utilisateur en session
    private function UserManagement(){
        $idUser = $this->session->get('identifiant');
        if(!isset($idUser)){
            $randomId = uniqid('user_');
            $idUser = $this->session->set('identifiant', $randomId);

            // persis le user
            return $randomId;
        }
        return $idUser;
    }

    /**
     * @Route("/api/salon/{idSalon}/updateLecteur", methods={"POST"})
     */
    public function AjaxGetLecteur(Request $request, $idSalon){
        $currentVideo = $this->getDoctrine()->getRepository(Video::class)->findOneBy(array('idSalon' => $idSalon));
        //dd($currentVideo);
        if ($currentVideo == NULL) {
            return new Response("[]");
        }
        $parameter = array();
        array_push($parameter, $currentVideo->getTimeCode());
        array_push($parameter, $currentVideo->getStatus());
        return new Response(json_encode($parameter));
    }

    /**
     * @Route("/api/salon/{id}/addMessage", methods={"POST"})
     */
    public function addMessage(Request $request, int $id) {

        $messageContent = $request->request->get('message');
        $idUser = $request->request->get('id_user');

        $targetChannel = $id;

        $repository = $this->getDoctrine()->getRepository(Salon::class);
        $currentChannel = $repository->find($targetChannel);

        $message = new Message();
        $message->setContent($messageContent);
        $message->setDateMessage(new \DateTime());
        $message->setUtilisateur($this->getDoctrine()->getRepository(Utilisateur::class)->find($idUser));

        $currentChannel->addMessage($message);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentChannel);
        $entityManager->flush();

        return new Response("OK");

    }

    /**
     * @Route("/api/salon/{id}/retrieveMessages", methods={"GET"})
     */
    public function retriveAllMessage(Request $request,int $id) {

        $repository = $this->getDoctrine()->getRepository(Salon::class);
        $currentChannel = $repository->find($id);

        if ($currentChannel == NULL) {
            return new Response("[]");
        }

        $messages = $currentChannel->getMessages();

        $res=[[]];

        for($i = 0; $i < count($messages); ++$i) {
            $res[$i]['id_user'] = $messages[$i]->getUtilisateur()->getId();
            $res[$i]['content'] = $messages[$i]->getContent();
            $res[$i]['pseudo'] = $messages[$i]->getUtilisateur()->getPseudo();
            $res[$i]['date'] = $this->formatDate($messages[$i]->getDateMessage()->format('Y-m-d H:i:s'));
        }

        return new Response(json_encode($res));

    }

    public function formatDate($date) {
        $dateCurrent = date("d",strtotime($date));
        if ($dateCurrent === date("d")) {
            $date = "Aujourd'hui - ".explode(" ", $date)[1];
        } else if ($dateCurrent === date('d',strtotime("-1 days"))) {
            $date="Hier - ".explode(" ", $date)[1];
        } else {
            $date=date_format(date_create($date), 'd-m-Y H:i:s');
        }
        return $date;
    }

    /**
     * @Route("/api/salon/{idSalon}/setLecteur", methods={"POST"})
     */
    public function AjaxSetLecteur(Request $request, int $idSalon){
        $timeCode = $request->request->get('timecode');
        $status = $request->request->get('status');
        $lien = $request->request->get('lien');
        //dump($lien);
        $currentVideo = $this->getDoctrine()->getRepository(Video::class)->findOneBy(array('idSalon' => $idSalon));
        if ($currentVideo == NULL) {
            return new Response("[]");
        }
        $currentVideo->setTimeCode($timeCode);
        $currentVideo->setStatus($status);
        $currentVideo->setLienVideo($lien);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentVideo);
        $entityManager->flush();
        return new Response();
    }


}