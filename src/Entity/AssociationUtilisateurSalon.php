<?php

namespace App\Entity;

use App\Repository\AssociationUtilisateurSalonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AssociationUtilisateurSalonRepository::class)
 */
class AssociationUtilisateurSalon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Utilisateur::class, inversedBy="associationUtilisateurSalons")
     */
    private $Utilisateur;

    /**
     * @ORM\ManyToMany(targetEntity=Salon::class, inversedBy="associationUtilisateurSalons")
     */
    private $Salon;

    public function __construct()
    {
        $this->Utilisateur = new ArrayCollection();
        $this->Salon = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateur(): Collection
    {
        return $this->Utilisateur;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->Utilisateur->contains($utilisateur)) {
            $this->Utilisateur[] = $utilisateur;
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        $this->Utilisateur->removeElement($utilisateur);

        return $this;
    }

    /**
     * @return Collection|Salon[]
     */
    public function getSalon(): Collection
    {
        return $this->Salon;
    }

    public function addSalon(Salon $salon): self
    {
        if (!$this->Salon->contains($salon)) {
            $this->Salon[] = $salon;
        }

        return $this;
    }

    public function removeSalon(Salon $salon): self
    {
        $this->Salon->removeElement($salon);

        return $this;
    }
}
