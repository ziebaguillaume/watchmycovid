<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Salon::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Channel;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Utilisateur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateMessage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChannel(): ?Salon
    {
        return $this->Channel;
    }

    public function setChannel(?Salon $Channel): self
    {
        $this->Channel = $Channel;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->Utilisateur;
    }

    public function setUtilisateur(?Utilisateur $Utilisateur): self
    {
        $this->Utilisateur = $Utilisateur;

        return $this;
    }

    public function getDateMessage(): ?\DateTimeInterface
    {
        return $this->DateMessage;
    }

    public function setDateMessage(\DateTimeInterface $DateMessage): self
    {
        $this->DateMessage = $DateMessage;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }
}
