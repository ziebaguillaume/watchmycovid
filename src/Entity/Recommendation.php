<?php

namespace App\Entity;

use App\Repository\RecommendationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecommendationRepository::class)
 */
class Recommendation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Lien;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\ManyToMany(targetEntity=AssociationTagRecommandation::class, mappedBy="Recommandation")
     */
    private $associationTagRecommandations;

    public function __construct()
    {
        $this->associationTagRecommandations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLien(): ?string
    {
        return $this->Lien;
    }

    public function setLien(string $Lien): self
    {
        $this->Lien = $Lien;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|AssociationTagRecommandation[]
     */
    public function getAssociationTagRecommandations(): Collection
    {
        return $this->associationTagRecommandations;
    }

    public function addAssociationTagRecommandation(AssociationTagRecommandation $associationTagRecommandation): self
    {
        if (!$this->associationTagRecommandations->contains($associationTagRecommandation)) {
            $this->associationTagRecommandations[] = $associationTagRecommandation;
            $associationTagRecommandation->addRecommandation($this);
        }

        return $this;
    }

    public function removeAssociationTagRecommandation(AssociationTagRecommandation $associationTagRecommandation): self
    {
        if ($this->associationTagRecommandations->removeElement($associationTagRecommandation)) {
            $associationTagRecommandation->removeRecommandation($this);
        }

        return $this;
    }
}
