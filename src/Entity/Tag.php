<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\ManyToMany(targetEntity=AssociationTagRecommandation::class, mappedBy="Tag")
     */
    private $associationTagRecommandations;

    public function __construct()
    {
        $this->associationTagRecommandations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    /**
     * @return Collection|AssociationTagRecommandation[]
     */
    public function getAssociationTagRecommandations(): Collection
    {
        return $this->associationTagRecommandations;
    }

    public function addAssociationTagRecommandation(AssociationTagRecommandation $associationTagRecommandation): self
    {
        if (!$this->associationTagRecommandations->contains($associationTagRecommandation)) {
            $this->associationTagRecommandations[] = $associationTagRecommandation;
            $associationTagRecommandation->addTag($this);
        }

        return $this;
    }

    public function removeAssociationTagRecommandation(AssociationTagRecommandation $associationTagRecommandation): self
    {
        if ($this->associationTagRecommandations->removeElement($associationTagRecommandation)) {
            $associationTagRecommandation->removeTag($this);
        }

        return $this;
    }
}
