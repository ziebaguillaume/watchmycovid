<?php

namespace App\Entity;

use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 */
class Utilisateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DistantCode;

    /**
     * @ORM\ManyToMany(targetEntity=AssociationUtilisateurSalon::class, mappedBy="Utilisateur")
     */
    private $associationUtilisateurSalons;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="Utilisateur", orphanRemoval=true)
     */
    private $messages;

    public function __construct()
    {
        $this->associationUtilisateurSalons = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->Pseudo;
    }

    public function setPseudo(?string $Pseudo): self
    {
        $this->Pseudo = $Pseudo;

        return $this;
    }

    public function getDistantCode(): ?string
    {
        return $this->DistantCode;
    }

    public function setDistantCode(string $DistantCode): self
    {
        $this->DistantCode = $DistantCode;

        return $this;
    }

    /**
     * @return Collection|AssociationUtilisateurSalon[]
     */
    public function getAssociationUtilisateurSalons(): Collection
    {
        return $this->associationUtilisateurSalons;
    }

    public function addAssociationUtilisateurSalon(AssociationUtilisateurSalon $associationUtilisateurSalon): self
    {
        if (!$this->associationUtilisateurSalons->contains($associationUtilisateurSalon)) {
            $this->associationUtilisateurSalons[] = $associationUtilisateurSalon;
            $associationUtilisateurSalon->addUtilisateur($this);
        }

        return $this;
    }

    public function removeAssociationUtilisateurSalon(AssociationUtilisateurSalon $associationUtilisateurSalon): self
    {
        if ($this->associationUtilisateurSalons->removeElement($associationUtilisateurSalon)) {
            $associationUtilisateurSalon->removeUtilisateur($this);
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUtilisateur($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getUtilisateur() === $this) {
                $message->setUtilisateur(null);
            }
        }

        return $this;
    }
}
