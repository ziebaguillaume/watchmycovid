<?php

namespace App\Repository;

use App\Entity\AssociationTagRecommandation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AssociationTagRecommandation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AssociationTagRecommandation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AssociationTagRecommandation[]    findAll()
 * @method AssociationTagRecommandation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssociationTagRecommandationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssociationTagRecommandation::class);
    }

    // /**
    //  * @return AssociationTagRecommandation[] Returns an array of AssociationTagRecommandation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AssociationTagRecommandation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
